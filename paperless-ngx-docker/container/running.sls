# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as paperless_ngx_docker with context %}

{%- set mountpoint = paperless_ngx_docker.docker.mountpoint %}

paperless-ngx-docker-mountdir-managed:
  file.directory:
    - name: {{ mountpoint }}/docker/paperless
    - makedirs: True

paperless-ngx-docker-broker-image-present:
  docker_image.present:
    - name: {{ paperless_ngx_docker.broker.image }}
    - tag: {{ paperless_ngx_docker.broker.tag }}
    - force: True

paperless-ngx-docker-db-image-present:
  docker_image.present:
    - name: {{ paperless_ngx_docker.db.image }}
    - tag: {{ paperless_ngx_docker.db.tag }}
    - force: True

paperless-ngx-docker-webserver-image-present:
  docker_image.present:
    - name: {{ paperless_ngx_docker.webserver.image }}
    - tag: {{ paperless_ngx_docker.webserver.tag }}
    - force: True

paperless-ngx-docker-gotenberg-image-present:
  docker_image.present:
    - name: {{ paperless_ngx_docker.gotenberg.image }}
    - tag: {{ paperless_ngx_docker.gotenberg.tag }}
    - force: True

paperless-ngx-docker-tika-image-present:
  docker_image.present:
    - name: {{ paperless_ngx_docker.tika.image }}
    - tag: {{ paperless_ngx_docker.tika.tag }}
    - force: True

paperless-ngx-docker-broker-container-running:
  docker_container.running:
    - name: {{ paperless_ngx_docker.broker.name }}
    - image: {{ paperless_ngx_docker.broker.image }}:{{ paperless_ngx_docker.broker.tag }}
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/paperless/redis:/data
    - require:
      - paperless-ngx-docker-broker-image-present

paperless-ngx-docker-db-container-running:
  docker_container.running:
    - name: {{ paperless_ngx_docker.db.name }}
    - image: {{ paperless_ngx_docker.db.image }}:{{ paperless_ngx_docker.db.tag }}
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/paperless/pgdata:/var/lib/postgresql/data
    - environment:
      - POSTGRES_DB: {{ paperless_ngx_docker.db.database_name }}
      - POSTGRES_USER: {{ paperless_ngx_docker.db.username }}
      - POSTGRES_PASSWORD: {{ paperless_ngx_docker.db.password }}
    - require:
      - paperless-ngx-docker-mountdir-managed
      - paperless-ngx-docker-db-image-present
      - paperless-ngx-docker-mountdir-managed

paperless-ngx-docker-gotenberg-container-running:
  docker_container.running:
    - name: {{ paperless_ngx_docker.gotenberg.name }}
    - image: {{ paperless_ngx_docker.gotenberg.image }}:{{ paperless_ngx_docker.gotenberg.tag }}
    - restart: unless-stopped
    - command: "gotenberg --chromium-disable-javascript=true --chromium-allow-list=file:///tmp/.*"
    - require:
      - paperless-ngx-docker-gotenberg-image-present

paperless-ngx-docker-tika-container-running:
  docker_container.running:
    - name: {{ paperless_ngx_docker.tika.name }}
    - image: {{ paperless_ngx_docker.tika.image }}:{{ paperless_ngx_docker.tika.tag }}
    - restart: unless-stopped
    - require:
      - paperless-ngx-docker-tika-image-present

paperless-ngx-docker-webserver-container-running:
  docker_container.running:
    - name: {{ paperless_ngx_docker.webserver.name }}
    - image: {{ paperless_ngx_docker.webserver.image }}:{{ paperless_ngx_docker.webserver.tag }}
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/paperless/data:/usr/src/paperless/data
      - {{ mountpoint }}/docker/paperless/media:/usr/src/paperless/media
      - {{ mountpoint }}/docker/paperless/export:/usr/src/paperless/export
      - {{ mountpoint }}/docker/paperless/consume:/usr/src/paperless/consume
    - environment:
      - PAPERLESS_REDIS: redis://{{ paperless_ngx_docker.broker.name }}:6379
      - USERMAP_UID: {{ paperless_ngx_docker.webserver.uid }}
      - USERMAP_GID: {{ paperless_ngx_docker.webserver.gid }}
      - PAPERLESS_URL: https://{{ paperless_ngx_docker.webserver.url }}
      - PAPERLESS_SECRET_KEY: {{ paperless_ngx_docker.webserver.secret_key }}
      - PAPERLESS_TIME_ZONE: {{ paperless_ngx_docker.webserver.timezone }}
      - PAPERLESS_OCR_LANGUAGE: {{ paperless_ngx_docker.webserver.ocr_language }}
      - PAPERLESS_DBHOST: {{ paperless_ngx_docker.db.name }}
      - PAPERLESS_DBNAME: {{ paperless_ngx_docker.db.database_name }}
      - PAPERLESS_DBUSER: {{ paperless_ngx_docker.db.username }}
      - PAPERLESS_DBPASS: {{ paperless_ngx_docker.db.password }}
      - PAPERLESS_TIKA_ENABLED: 1
      - PAPERLESS_TIKA_GOTENBERG_ENDPOINT: http://{{ paperless_ngx_docker.gotenberg.name }}:3000
      - PAPERLESSS_TIKA_ENDPOINT: http://{{ paperless_ngx_docker.tika.name }}:9998
    - port_bindings:
      - 8081:8080
    - links:
      - postgres: postgres
      - gotenberg: gotenberg
      - tika: tika
      - redis: redis
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.paperless-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.paperless-web.middlewares=paperless-redirect-websecure"
      - "traefik.http.routers.paperless-web.rule=Host(`{{ paperless_ngx_docker.webserver.url }}`)"
      - "traefik.http.routers.paperless-web.entrypoints=web"
      - "traefik.http.routers.paperless-websecure.rule=Host(`{{ paperless_ngx_docker.webserver.url }}`)"
      - "traefik.http.routers.paperless-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.paperless-websecure.tls=true"
      - "traefik.http.routers.paperless-websecure.entrypoints=websecure"
    - require:
      - paperless-ngx-docker-mountdir-managed
      - paperless-ngx-docker-webserver-image-present
      - paperless-ngx-docker-broker-container-running
      - paperless-ngx-docker-db-container-running
      - paperless-ngx-docker-gotenberg-container-running
      - paperless-ngx-docker-tika-container-running
