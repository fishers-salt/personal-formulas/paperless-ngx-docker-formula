# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_clean = tplroot ~ '.config.clean' %}
{%- from tplroot ~ "/map.jinja" import mapdata as paperless_ngx_docker with context %}

paperless-ngx-docker-webserver-container-absent:
  docker_container.absent:
    - name: {{ paperless_ngx_docker.webserver.name }}

paperless-ngx-docker-tika-container-absent:
  docker_container.absent:
    - name: {{ paperless_ngx_docker.tika.name }}

paperless-ngx-docker-gotenberg-container-absent:
  docker_container.absent:
    - name: {{ paperless_ngx_docker.gotenberg.name }}

paperless-ngx-docker-db-container-absent:
  docker_container.absent:
    - name: {{ paperless_ngx_docker.db.name }}

paperless-ngx-docker-broker-container-absent:
  docker_container.absent:
    - name: {{ paperless_ngx_docker.broker.name }}

paperless-ngx-docker-tika-image-absent:
  docker_image.absent:
    - name: {{ paperless_ngx_docker.tika.image }}

paperless-ngx-docker-gotenberg-image-absent:
  docker_image.absent:
    - name: {{ paperless_ngx_docker.gotenberg.image }}

paperless-ngx-docker-webserver-image-absent:
  docker_image.absent:
    - name: {{ paperless_ngx_docker.webserver.image }}

paperless-ngx-docker-db-image-absent:
  docker_image.absent:
    - name: {{ paperless_ngx_docker.db.image }}

paperless-ngx-docker-broker-image-absent:
  docker_image.absent:
    - name: {{ paperless_ngx_docker.broker.image }}
