# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_container_running = tplroot ~ '.container.running' %}
{%- from tplroot ~ "/map.jinja" import mapdata as paperless_ngx_docker with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_container_running }}

{%- set mountpoint = paperless_ngx_docker.docker.mountpoint %}

paperless-config-file-superuser-created:
  docker_container.run:
    - image: {{ paperless_ngx_docker.webserver.image }}
    - command: createsuperuser --noinput
    - binds:
      - {{ mountpoint }}/docker/paperless/data:/usr/src/paperless/data
      - {{ mountpoint }}/docker/paperless/media:/usr/src/paperless/media
      - {{ mountpoint }}/docker/paperless/export:/usr/src/paperless/export
      - {{ mountpoint }}/docker/paperless/consume:/usr/src/paperless/consume
    - creates: {{ mountpoint }}/docker/paperless/.superuser-created
    - environment:
      - PAPERLESS_REDIS: redis://{{ paperless_ngx_docker.broker.name }}:6379
      - USERMAP_UID: {{ paperless_ngx_docker.webserver.uid }}
      - USERMAP_GID: {{ paperless_ngx_docker.webserver.gid }}
      - PAPERLESS_URL: https://{{ paperless_ngx_docker.webserver.url }}
      - PAPERLESS_SECRET_KEY: {{ paperless_ngx_docker.webserver.secret_key }}
      - PAPERLESS_TIME_ZONE: {{ paperless_ngx_docker.webserver.timezone }}
      - PAPERLESS_OCR_LANGUAGE: {{ paperless_ngx_docker.webserver.ocr_language }}
      - PAPERLESS_DBHOST: {{ paperless_ngx_docker.db.name }}
      - PAPERLESS_DBNAME: {{ paperless_ngx_docker.db.database_name }}
      - PAPERLESS_DBUSER: {{ paperless_ngx_docker.db.username }}
      - PAPERLESS_DBPASS: {{ paperless_ngx_docker.db.password }}
      - PAPERLESS_TIKA_ENABLED: 1
      - PAPERLESS_TIKA_GOTENBERG_ENDPOINT: http://{{ paperless_ngx_docker.gotenberg.name }}:3000
      - PAPERLESSS_TIKA_ENDPOINT: http://{{ paperless_ngx_docker.tika.name }}:9998
      - DJANGO_SUPERUSER_USERNAME: {{ paperless_ngx_docker.webserver.superuser_username }}
      - DJANGO_SUPERUSER_PASSWORD: {{ paperless_ngx_docker.webserver.superuser_password }}
      - DJANGO_SUPERUSER_EMAIL: {{ paperless_ngx_docker.webserver.superuser_email }}
    - links:
      - postgres: postgres
      - gotenberg: gotenberg
      - tika: tika
      - redis: redis
    - require:
      - sls: {{ sls_container_running }}

paperless-config-file-superuser-file-created:
  file.managed:
    - name: {{ mountpoint }}/docker/paperless/.superuser-created
    - require:
      - paperless-config-file-superuser-created
