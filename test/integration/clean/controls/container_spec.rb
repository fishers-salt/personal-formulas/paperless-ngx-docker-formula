# frozen_string_literal: true

describe docker_container(name: 'redis') do
  it { should_not exist }
end

describe docker_container(name: 'postgres') do
  it { should_not exist }
end

describe docker_container(name: 'gotenberg') do
  it { should_not exist }
end

describe docker_container(name: 'tika') do
  it { should_not exist }
end

describe docker_container(name: 'paperless') do
  it { should_not exist }
end
