# frozen_string_literal: true

control 'paperless-config-file-superuser-file-created' do
  title 'superuser should be created'

  describe file('/srv/docker/paperless/.superuser-created') do
    it { should exist }
    it { should be_file }
  end
end
