# frozen_string_literal: true

describe docker_container(name: 'redis') do
  it { should exist }
  it { should be_running }
  its('image') { should eq 'docker.io/library/redis:7' }
  it { should have_volume('/data', '/srv/docker/paperless/redis') }
end

describe docker_container(name: 'postgres') do
  it { should exist }
  it { should be_running }
  its('image') { should eq 'docker.io/library/postgres:13' }
  it { should have_volume('/var/lib/postgresql/data', '/srv/docker/paperless/pgdata') }
end

describe docker_container(name: 'gotenberg') do
  it { should exist }
  it { should be_running }
  its('image') { should eq 'docker.io/gotenberg/gotenberg:7.8' }
end

describe docker_container(name: 'tika') do
  it { should exist }
  it { should be_running }
  its('image') { should eq 'ghcr.io/paperless-ngx/tika:latest' }
end

describe docker_container(name: 'paperless') do
  it { should exist }
  it { should be_running }
  its('image') { should eq 'ghcr.io/paperless-ngx/paperless-ngx:latest' }
  it { should have_volume('/usr/src/paperless/data', '/srv/docker/paperless/data') }
  it { should have_volume('/usr/src/paperless/media', '/srv/docker/paperless/media') }
  it { should have_volume('/usr/src/paperless/export', '/srv/docker/paperless/export') }
  it do
    should have_volume('/usr/src/paperless/consume', '/srv/docker/paperless/consume')
  end
end
